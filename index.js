function toggleState() {
  const checked = document.getElementById("plan-toggle").checked;

  if (checked === true) {
    document.getElementsByClassName("pricing-basic")[0].textContent = "199.99";
    document.getElementsByClassName("pro")[0].textContent = "249.99";
    document.getElementsByClassName("pricing-master")[0].textContent = "399.99";
  } else {
    document.getElementsByClassName("pricing-basic")[0].textContent = "19.99";
    document.getElementsByClassName("pro")[0].textContent = "24.99";
    document.getElementsByClassName("pricing-master")[0].textContent = "39.99";
  }
}
